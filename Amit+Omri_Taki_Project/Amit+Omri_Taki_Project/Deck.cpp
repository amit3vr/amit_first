#include <algorithm>
#include <vector>
#include "Deck.h"
#include "Card.h"
#include "TakiCore.h"

using namespace TakiConstants::CardConfig;

Deck::Deck()
{
	for (int i = 0; i < 2; i++)
	{
		for (COLOR color = COLOR::RED; color <= COLOR::YELLOW; color = COLOR(color + 1))
		{
			for (auto type = '1'; type <= '9'; type++)
			{
				this->_deck.push_back(Card(color, type));
			}

			for (TYPE type = TYPE::PLUS; type <= TYPE::SUPER_TAKI; type = TYPE(type + 1))
			{
				if (type != TYPE::CHNG_CLR && type != TYPE::SUPER_TAKI)
					this->_deck.push_back(Card(color, type));
			}
		}

		for (int j = 0; j < 2; j++)
		{
			this->_deck.push_back(Card(COLOR::UNDEF, TYPE::CHNG_CLR));
			this->_deck.push_back(Card(COLOR::UNDEF, TYPE::SUPER_TAKI));
		}
	}

	this->shuffle();

	// CHECK THAT THE OPENNING CARD IS NOT OF ANY SPECIAL TYPE!
	this->place(this->draw());
}

void Deck::shuffle()
{
	std::random_shuffle(this->_deck.begin(), this->_deck.end());
}

Card Deck::current() const
{
	return Card(*(--this->_openned.end()));
}

Card Deck::draw()
{
	if (this->_deck.empty())
	{
		this->_deck.push_back(this->current());
		this->_openned.pop_back();

		this->_deck.swap(this->_openned);
		this->shuffle();
	}

	Card c(*(--this->_deck.end()));

	this->_deck.pop_back();
	return c;
}

void Deck::place(Card& c)
{
	this->_openned.push_back(c);
}

Deck::~Deck()
{
	this->_deck.clear();
	this->_openned.clear();
}