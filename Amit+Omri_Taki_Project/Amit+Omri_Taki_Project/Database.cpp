#include <iostream>
#include <string>
#include "TakiCore.h"
#include "sqlite3.h"

using namespace std;
using namespace TakiDB;

Database::Database(string db_file_name, bool restart, bool debug) : _db(NULL)
{
	this->_restart = restart;
	this->_debug = debug;

	this->_status = (sqlite3_open(db_file_name.c_str(), &(this->_db)) == SQLITE_OK);
}

int Database::parse(void* _parser_, int _fields_, char** _rows_, char** _columns_)
{
	query_parsed_record record;

	for (int i = 0; i < _fields_; i++)
	{
		record.insert(query_parsed_record::value_type(
						_columns_[i], /* field name */
						(_rows_[i] ? _rows_[i] : "") /* field value */
					));
	}

	((query_parsed_package*)(_parser_))
		->insert(query_parsed_package::value_type
					(
						(record.find("id") != record.end()) ?
						atoi(record.find("id")->first.c_str()) : -1,
						record
					)
				);

	return 0;
}

bool Database::exec(string query, query_parsed_package* parser)
{
	sqlite3_callback query_parser = parser ? Database::parse : NULL;
	bool success;

	success =	(this->_status &&
				(sqlite3_exec(this->_db, query.c_str(), query_parser, parser, NULL)
					== SQLITE_OK)
				);

	if (this->_debug)
	{
		cout << "> Query: \"" << query << '"' << endl;

		if (success)
			cout << "  was preformed successfully." << endl;
		else
			cout << "  failed with error: " << sqlite3_errmsg(this->_db) << endl;

		cout << '\n';
	}

	return success;
}

sqlite3_int64 Database::last_insert_id() const
{
	return sqlite3_last_insert_rowid(this->_db);
}

int Database::errcode() const
{
	return sqlite3_errcode(this->_db);
}

string Database::errmsg() const
{
	return string(sqlite3_errmsg(this->_db));
}

Database::~Database()
{
	sqlite3_close(this->_db);

	if (this->_restart)
		remove(TakiConfig::DB_FILE_NAME.c_str());
}