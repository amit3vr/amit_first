#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <Windows.h>
#include <thread>
#include "sqlite3.h"
#include "TakiCore.h"
#include "TakiModules.h"
#include "Room.h"
#include "User.h"

using namespace std;
using namespace TakiConstants;
using namespace TakiModules;
using namespace TakiAPI;
using namespace TakiDB;

Server::Server() : _socket(INVALID_SOCKET), _db(NULL)
{
	if (!this->init_server_database())
	{
		cout << "> ERROR: Failed initiating server database. exiting." << endl;
		goto exit_error;
	}

	if (!this->init_server_socket())
	{
		cout << "ERROR: Failed initiating server socket. exiting." << endl;
		goto exit_error;
	}

	cout << "SERVER STATUS: ON" << endl;
	cout << "now listening for pending connections..\n" << endl;

	listen(this->_socket, TakiConfig::MAX_CONNECTIONS);

	// Server is set.
	// Run Taki.
	thread* new_client_thread;

	struct sockaddr_in client_details;
	SOCKET new_client_sock;
	
	while (TakiConfig::MAX_CONNECTIONS ? 
				this->_clients.size() < TakiConfig::MAX_CONNECTIONS : true)
	{
		new_client_sock = accept(this->_socket, (struct sockaddr *) &client_details, NULL);

		if (new_client_sock != INVALID_SOCKET)
		{
			cout << "> " << inet_ntoa(client_details.sin_addr) << " has connected to server ";
			cout << "as socket id: " << new_client_sock << endl;

			// lambda function,
			// reference @ http://stackoverflow.com/questions/10673585/start-thread-with-member-function
			new_client_thread = new thread([this](SOCKET s) 
								{
									this->init_client_procces(s);
								}
								, new_client_sock);

			this->_clients
					.insert(client_proc_package(new_client_sock,new_client_thread))
					.first->	/* iterator of the newly inserted client */
					second->	/* the second value stored in the client's info package,
								** which is the pointer of the client's thread */
					detach();
		}
	}

	exit_error: EOF;
}

bool Server::init_server_database()
{
	string query;

	this->_db = new Database(TakiConfig::DB_FILE_NAME);

	/* Allow foreign keys */
	this->_db->exec("PRAGMA foreign_keys = ON");

	/* Create database tables */
	query = "create table if not exists users("  \
				"user_id integer primary key autoincrement," \
				"username text not null unique," \
				"password text not null" \
		");";

	if (!this->_db->exec(query)) return false;

	query = "create table if not exists games("  \
				"game_id integer primary key autoincrement," \
				"game_start integer," \
				"game_end integer," \
				"turns_number integer" \
			");";

	if (!this->_db->exec(query)) return false;

	query = "create table if not exists user_games("  \
				"game_id integer not null," \
				"user_id integer not null," \
				"is_winner bool not null," \
				"primary key(game_id, user_id)," \
				"foreign key(game_id) references games(game_id)," \
				"foreign key(user_id) references users(user_id)" \
			");";

	if (!this->_db->exec(query)) return false;

	return true;
}

bool Server::init_server_socket()
{
	// Initiate server
	WSADATA info;

	if (WSAStartup(MAKEWORD(2, 0), &info) != 0)
	{
		cout << "WSAStartup failed with error: " << WSAGetLastError() << endl;
		return false;
	}

	if ((this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
	{
		cout << "Socket formation failed with error code: " << WSAGetLastError() << endl;
		return false;
	}

	struct sockaddr_in _server;

	_server.sin_family = AF_INET;
	_server.sin_addr.s_addr = inet_addr((TakiDB::TakiConfig::HOST_IP).c_str());
	_server.sin_port = htons(TakiDB::TakiConfig::PORT);

	if (::bind(this->_socket, (struct sockaddr *)&_server, sizeof(_server)) == SOCKET_ERROR)
	{
		cout << "Binding socket to server failed with error: " << WSAGetLastError() << endl;
		return false;
	}

	return true;
}

void Server::init_client_procces(SOCKET s)
{
	char cmd[TakiDB::TakiConfig::MAX_COMMAND_SIZE] = "";
	int bytes;

	do
	{
		memset(cmd, 0, TakiDB::TakiConfig::MAX_COMMAND_SIZE);

		// Waiting for client request..
		bytes = recv(s, cmd, TakiConfig::MAX_COMMAND_SIZE - 1, 0);

		if (bytes > 0)
		{
			this->run_client_procces_cmd(s, cmd);
		}
		else if (bytes == 0)
		{
			//Command(PGM_MER_MESSAGE).send_to(s);
		}
		else if (bytes == SOCKET_ERROR)
		{
			cout << "> Client (socket id:" << s << ") has disconnected from the server." << endl;
			this->disconnect_client(s);
		}
	}
	while (bytes > 0);
}

bool Server::run_client_procces_cmd(SOCKET s, string cmd)
{
	cmd_parsed_package parsed_cmd = Command(cmd).parse();

	cmd_id commandID = parsed_cmd.first;
	vector<string>& args = parsed_cmd.second;

	cout << "> CMD from socket " << s << ": \"" << cmd << '"' << endl;

	switch (commandID)
	{
		/* UNDEFINED/UNAUTHORIZED CMD */
		default:
			Command(Errors::PGM_MER_MESSAGE).send_to(s);
		break;

		/* REQUESTS */
		case Requests::EN_REGISTER:
			if (args.size() != 2)
				Command(Errors::PGM_ERR_REGISTERR_INFO).send_to(s);
			else
				return this->user_register(s, args[0], args[1]);
		break;

		case Requests::EN_LOGIN:
			if (args.size() != 2)
				Command(Errors::PGM_ERR_LOGIN).send_to(s);
			else
				return this->user_login(s, args[0], args[1]);
		break;

		case Requests::EN_LOGOUT:
			return this->user_logout(s);
		break;

		case Requests::RM_CREATE_GAME:
			if (args.size() != 1)
				Command(Errors::PGM_ERR_GAME_CREATED).send_to(s);
			else
				return this->create_room(s, args[0]);
		break;

		case Requests::RM_JOIN_GAME:
			if (args.size() != 1)
				Command(Errors::PGM_ERR_ROOM_NOT_FOUND).send_to(s);
			else
				return this->join_room(s, args[0]);
		break;

		case Requests::RM_START_GAME:
			return this->modify(s, true);
		break;

		case Requests::RM_LEAVE_GAME:
			return this->leave_room(s);
		break;

		case Requests::RM_CLOSE_GAME:
			return this->modify(s, false);
		break;

		case Requests::RM_ROOM_LIST:
			return this->room_lister(s);
		break;

		case Requests::GM_PLAY:
			return this->make_move(s, args);
		break;
	}

	return false;
}

bool Server::user_register(SOCKET s, string username, string password)
{
	string query;

	if (this->_logged_users.find(s) != this->_logged_users.end())
	{
		Command(Errors::PGM_ERR_LOGIN,
			string("You are already connected as ") \
			+ _logged_users.find(s)->second.username()).send_to(s);

		return false;
	}
	else if ((username.length() > UserConfig::MAX_UNAME_SIZE) ||
			(password.length() > UserConfig::MAX_PASSW_SIZE))
	{
		Command(Errors::PGM_ERR_INFO_TOO_LONG).send_to(s);

		return false;
	}

	query = "insert into users(username, password) " \
			"values(\"" + username + "\", \"" + password + "\");";

	if (!this->_db->exec(query))
	{
		if (this->_db->errcode() == SQLITE_CONSTRAINT_UNIQUE)
			Command(Errors::PGM_ERR_NAME_TAKEN).send_to(s);
		else
			Command(Errors::PGM_ERR_REGISTERR_INFO).send_to(s);

		return false;
	}
	else
	{
		Command(Responses::PGM_SCC_REGISTER).send_to(s);
		this->_logged_users.insert(pair<SOCKET, User>(s, User(s, username)));
	}

	return true;
}

bool Server::user_login(SOCKET s, string username, string password)
{
	string query;
	query_parsed_package prs;

	if (this->_logged_users.find(s) != this->_logged_users.end())
	{
		Command(Errors::PGM_ERR_LOGIN,
				string("You are already connect as ") \
				+ _logged_users.find(s)->second.username()).send_to(s);

		return false;
	}
	else if ((username.length() > UserConfig::MAX_UNAME_SIZE) ||
			(password.length() > UserConfig::MAX_PASSW_SIZE))
	{
		Command(Errors::PGM_ERR_INFO_TOO_LONG).send_to(s);

		return false;
	}
	else
	{
		for each(pair<SOCKET, User> user in this->_logged_users)
		{
			if (user.second.username().compare(username) == 0)
			{
				Command(Errors::PGM_ERR_LOGIN,
						"User is already connected to the server").send_to(s);

				return false;
			}
		}
	}

	query = "select *  from users where username = '" + username  + "' "\
		"and password = '" + password + "';";

	if (!this->_db->exec(query, &prs) || prs.empty())
	{
		Command(Errors::PGM_ERR_LOGIN, "User not found").send_to(s);

		return false;
	}
	else
	{
		Command(Responses::PGM_SCC_LOGIN).send_to(s);
		this->_logged_users.insert(pair<SOCKET, User>(s, User(s, username)));
	}
	
	return true;
}

bool Server::user_logout(SOCKET s)
{
	Command().send_to(s);
	
	return (this->_logged_users.erase(s) == 0);
}

bool Server::make_move(SOCKET s, vector<string>& args)
{
	vector<Card> moves;

	if (this->_logged_users.find(s) == this->_logged_users.end())
	{
		Command(Errors::PGM_ERR_LOGIN).send_to(s);
		return false;
	}

	if (!this->_logged_users.find(s)->second.room())
	{
		Command(Errors::PGM_ERR_ROOM_NOT_FOUND).send_to(s);
		return false;
	}

	for each (string arg in args)
	{
		if (arg.size() == 2)
		{
			moves.push_back(Card(arg.at(0), arg.at(1)));
		}
		else
		{
			Command(Errors::GAM_ERR_ILLEGAL_CARD).send_to(s);
		}
	}

	try
	{
		this->_logged_users.find(s) /* get iterator to the user */
							->second /* get the second value of the result (User instance) */
			.				room()->play_turn(s, moves);

		return true;
	}

	catch (cmd_id e)
	{
		Command(e).send_to(s);
	}

	return false;
}

bool Server::room_lister(SOCKET s) const
{
	string room_details;

	if (this->_rooms.empty())
	{
		Command(Errors::PGM_ERR_ROOM_NOT_FOUND).send_to(s);
		return false;
	}

	for each (pair<string, Room> room in this->_rooms)
	{
		Command(Responses::PGM_CTR_ROOM_LIST,
				room.second.name().c_str(),
				room.second.admin()->username().c_str(),
				to_string(room.second.num_of_players()).c_str()	
			).send_to(s);
	}

	return true;
}

bool Server::create_room(SOCKET s, string name)
{
	if (this->_logged_users.find(s) == this->_logged_users.end())
	{
		Command(Errors::PGM_ERR_LOGIN).send_to(s);
		return false;
	}

	if (name.size() > TakiConstants::RoomConfig::MAX_NAME_SIZE)
	{
		Command(Errors::PGM_ERR_INFO_TOO_LONG).send_to(s);
		return false;
	}

	this->_logged_users.find(s)->second
		.enter(
			&this->_rooms.insert(pair<string, Room>
								(name, Room(name))
								).first		/* the iterator to the insert result */
								->second	/* the new room object */
			);

	Command(Responses::PGM_SCC_GAME_CREATED).send_to(s);
	return true;
}

bool Server::join_room(SOCKET s, string name)
{
	try
	{
		if (this->_logged_users.find(s) == this->_logged_users.end())
			throw(Errors::PGM_ERR_LOGIN);

		if (this->_logged_users.find(s)->second.room())
			throw(Errors::PGM_ERR_LOGIN);

		if (this->_rooms.find(name) == this->_rooms.end())
			throw(Errors::PGM_ERR_ROOM_NOT_FOUND);

		this->_rooms.find(name)->second
			.add(this->_logged_users.find(s)->second);

	}
	catch (cmd_id errno)
	{
		Command(errno).send_to(s);
		return false;
	}

	Command(Responses::PGM_SCC_GAME_JOIN).send_to(s);
	return true;
}

bool Server::leave_room(SOCKET s)
{
	try
	{
		if (this->_logged_users.find(s) == this->_logged_users.end())
			throw(Errors::PGM_ERR_LOGIN);

		if (!this->_logged_users.find(s)->second.exit())
			throw(Errors::PGM_ERR_ROOM_NOT_FOUND);
	}
	catch (cmd_id errno)
	{
		Command(errno).send_to(s);
	}

	return true;
}

bool Server::modify(SOCKET s, bool flag)
{
	User* user;
	string query;

	try
	{
		if (this->_logged_users.find(s) == this->_logged_users.end())
			throw(Errors::PGM_ERR_LOGIN);

		if (!(user = &this->_logged_users.find(s)->second)->is_admin())
			throw(Errors::PGM_MER_ACCESS);

		if (flag)
		{
			user->room()->start_game();

			query = "insert into games(game_start) values(strftime('%s','now'));";
			this->_db->exec(query);

			user->room()->set_game_id((unsigned int)this->_db->last_insert_id());

			Command(Responses::PGM_SCC).send_to(s);
		}
		else
		{
			user->room()->end_game();

			query = "update games set game_end = strftime('%s','now'), " \
					"turns_number = '" + to_string(user->room()->turns_number()) + "' " \
					"where game_id = '" + to_string(user->room()->game_id()) + "';";

			this->_db->exec(query);

			Command(Responses::PGM_SCC_GAME_CLOSE).send_to(s);
		}
	}
	catch (cmd_id errno)
	{
		Command(errno).send_to(s);
		return false;
	}

	return true;
}

void Server::disconnect_client(SOCKET clientID)
{
	this->_logged_users.erase(clientID);

	if(this->_clients.erase(clientID))
		closesocket(clientID);
}

Server::~Server()
{
	// Close DB association
	delete this->_db;

	// Shutdown server
	for each (client_proc_package client in this->_clients)
		closesocket(client.first);
	
	closesocket(this->_socket);
	WSACleanup();

	// Clean Data
	this->_clients.clear();
	this->_logged_users.clear();
	this->_rooms.clear();
}