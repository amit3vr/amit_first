#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <thread>
#include <chrono>
#include "TakiModules.h"
#include "TakiCore.h"
#include "Room.h"
#include "User.h"

using namespace TakiModules;
using namespace TakiAPI;
using namespace TakiDB;

using std::cout;
using std::cin;
using std::endl;

DemoClient::DemoClient()
{
	char	buffer[TakiDB::TakiConfig::MAX_COMMAND_SIZE];
	string	cmd;
	int		bytes;

	if (!this->init_demo_client_socket())
		goto exit_error;

	cout << "Welcome to the Demo Client Test Program." << endl;
	cout << "Start by sending valid commands to the server.\n" << endl;

	cout << "Command Template: @[cmdID]|({opt}[arg_1]|)({opt}[arg_2]|)({opt}[arg_N]|)|" << endl;
	cout << "Command Example: \"@1|amit|12345||\" " \
		"=> registers user 'amit' with password '12345' into the system.\n\n" << endl;
	
	do
	{
		// Reset buffers
		memset(buffer, 0, TakiDB::TakiConfig::MAX_COMMAND_SIZE);
		cmd.clear();

		// Input command
		cout << "TakiServer << ";
		cin >> cmd;

		// Send command to server..
		Command(cmd).send_to(this->_socket);

		// Wait for server response..
		bytes = recv(this->_socket, buffer, TakiDB::TakiConfig::MAX_COMMAND_SIZE - 1, 0);

		if (bytes > 0)
		{
			cout << "TakiServer >> " << buffer << endl;
		}
	}
	while (cmd.compare(".quit") != 0 && bytes > 0);

	exit_error: cout << "Socket Error: " << WSAGetLastError() << endl;
}

bool DemoClient::init_demo_client_socket()
{
	WSADATA info;
	if (WSAStartup(MAKEWORD(2, 0), &info))
	{
		cout << "WSAStartup has failed with error: " << WSAGetLastError() << endl;
		return false;
	}

	if ((this->_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
	{
		cout << "Socket formation failed with error code: " << WSAGetLastError() << endl;
		return false;
	}

	struct sockaddr_in server_details;
	server_details.sin_family = AF_INET;
	server_details.sin_addr.s_addr = inet_addr(TakiConfig::HOST_IP.c_str());
	server_details.sin_port = htons(TakiConfig::PORT);

	if (connect(this->_socket, (struct sockaddr*)(&server_details), sizeof(server_details))
		== SOCKET_ERROR)
	{
		cout << "Connecting to server failed with error: " << WSAGetLastError() << endl;
		return false;
	}

	return true;
}

DemoClient::~DemoClient()
{
	closesocket(this->_socket);
	WSACleanup();
}