#ifndef _DECK
#define _DECK

#include <vector>
#include "Card.h"

using std::vector;

class Deck
{
	private:
		vector<Card> _deck;
		vector<Card> _openned;

		void shuffle();

	public:
		Deck();
		~Deck();

		Card current() const;

		Card draw();
		void place(Card& c);
};

#endif