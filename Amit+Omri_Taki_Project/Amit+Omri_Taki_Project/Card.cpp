#include <iostream>
#include "TakiCore.h"
#include "Card.h"

using namespace TakiConstants::CardConfig;

Card::Card(char color, char type)
{
	this->_color = color;
	this->_type = type;
}

bool Card::set_color(char color)
{
	if (this->_color == COLOR::UNDEF)
	{
		this->_color = color;

		return true;
	}

	return false;
}

char Card::get_color() const
{
	return this->_color;
}

char Card::get_type() const
{
	return this->_type;
}

bool Card::operator< (const Card& compared) const
{
	return (this->get_type() < compared.get_type());
}

bool Card::operator> (const Card& compared) const
{
	return (!((*this) < compared));
}

bool Card::operator== (const Card& compared) const
{
	if (this->get_type() == TYPE::DOUBLE &&
			this->get_type() == compared.get_type())
	{
		return true;
	}

	if (this->get_color() == compared.get_color() ||
			this->get_type() == compared.get_type())
	{
		return true;
	}

	return false;
}

bool Card::operator!= (const Card& compared) const
{
	return (!((*this) == compared));
}