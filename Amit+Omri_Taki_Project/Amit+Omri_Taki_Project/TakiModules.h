#ifndef _TAKIMODS
#define _TAKIMODS

#include <iostream>
#include <Windows.h>
#include <thread>
#include <vector>
#include <map>
#include "TakiCore.h"
#include "sqlite3.h"
#include "Room.h"
#include "User.h"

namespace TakiModules
{
	namespace
	{
		using std::map;
		using std::string;

		using TakiDB::Database;
	}

	class Server
	{
		using client_id = TakiAPI::client_proc_package::first_type;
		using client_proc = TakiAPI::client_proc_package::second_type;

		private:
												// WARNING! Solve it because it can lead to hacking.
			SOCKET				_socket;		// EX: Writing another file with commands that cheat the system.	
			map<string, Room>	_rooms;
			map<SOCKET, User>	_logged_users;
			map<client_id, client_proc> 
								_clients;
			Database*			_db;

			/* server initiators */
			bool init_server_database();
			bool init_server_socket();

			/* client management */
			void init_client_procces(SOCKET s);
			bool run_client_procces_cmd(SOCKET s, string cmd);

			void disconnect_client(SOCKET clientID);

			/* requests */
			bool user_register(SOCKET s, string username, string password);
			bool user_login(SOCKET s, string username, string password);
			bool user_logout(SOCKET s);

			bool room_lister(SOCKET s) const;

			bool create_room(SOCKET s, string name);
			bool join_room(SOCKET s, string name);
			bool leave_room(SOCKET s);

			bool modify(SOCKET s, bool flag);
			bool make_move(SOCKET s, vector<string>& args);

		public:
			Server();
			~Server();
	};

	class DemoClient
	{
		private:
			SOCKET _socket;

			bool init_demo_client_socket();

		public:
			DemoClient();
			~DemoClient();
	};
}

#endif