#ifndef _USER
#define _USER

#include <iostream>
#include <Windows.h>
#include "TakiCore.h"
#include "Card.h"

/* forward-decleration dependency */
#ifndef _ROOM
	class Room;
#endif

/* extentions */
using std::string;

class User
{
	private:
		SOCKET			_socket;
		string			_username;
		Room*			_room;

	public:
		User(SOCKET socket, string username);
		~User();

		bool enter(Room* room);
		bool exit();

		SOCKET	socket() const;
		string	username() const;
		Room*	room() const;
		bool	is_admin() const;

		bool operator== (const User& compared);
		bool operator!= (const User& compared);
};

#endif