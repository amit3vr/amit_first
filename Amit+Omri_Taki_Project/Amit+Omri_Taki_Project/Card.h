#ifndef _CARD
#define _CARD

#include "TakiCore.h"

using TakiConstants::CardConfig::COLOR;
using TakiConstants::CardConfig::TYPE;

class Card
{
	private:
		char	_color;
		char	_type;

	public:
		Card(char color, char type);
		~Card() {};

		bool set_color(char);

		char get_color() const;
		char get_type() const;

		bool operator< (const Card& compared) const;
		bool operator> (const Card& compared) const;

		bool operator== (const Card& compared) const;
		bool operator!= (const Card& compared) const;
};

#endif