#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <regex>
#include <string>
#include <Windows.h>
#include "TakiCore.h"

using std::string;
using std::vector;
using std::regex;

using namespace TakiAPI;
using namespace TakiConstants;

Command::Command(cmd_id ID, const char* first, ...)
{
	va_list args;
	va_start(args, ID);

	this->init(ID, args, CMDParser::END_CMD);
}

Command::Command(cmd_id ID, string first, ...)
{
	va_list args;
	va_start(args, first);

	Command(ID, first.c_str(), args, CMDParser::END_CMD);
}

Command::Command(cmd_id ID)
{
	this->init(ID, CMDParser::END_CMD);
}

Command::Command(string cmd)
{
	if (regex_match(cmd, CMDParser::CMD_TEMPLATE))
	{
		this->_command = cmd;
	}
	else
	{
		this->init(Errors::PGM_MER_MESSAGE, CMDParser::END_CMD);
	}
}

void Command::init(cmd_id ID, ...)
{
	va_list args;
	va_start(args, ID);

	char* next_arg;

	this->_command = '@' + std::to_string(ID) + '|';

	while ((next_arg = va_arg(args, char*)) != CMDParser::END_CMD)
	{
		this->_command += string(next_arg);
		this->_command += '|';
	}

	this->_command += '|';
	va_end(args);
}

bool Command::send_to(SOCKET s) const
{
	int bytes = 0;

	if (s == INVALID_SOCKET)
	{
		return false;
	}

	if (this->_command.length() > TakiDB::TakiConfig::MAX_COMMAND_SIZE)
	{
		Command(CMDParser::CMD_ERROR).send_to(s);
		return false;
	}

	bytes = 
		send(
				s,
				this->_command.c_str(),
				this->_command.length(),
				0
		);

	return (bytes > 0);
}

cmd_parsed_package Command::parse()
{
	string cmd = this->_command;
	string arg;

	std::smatch arg_matches;
	bool input_cmd_id = false;

	vector<string> args;
	cmd_id ID = CMDParser::CMD_UNDEFINED;

	if (regex_match(cmd, CMDParser::CMD_TEMPLATE))
	{
		std::sregex_token_iterator match(cmd.begin(), cmd.end(), CMDParser::CMD_ARG, 1);
		std::sregex_token_iterator endd;

		while(match != endd)
		{
			if (!input_cmd_id)
			{
				ID = stoi(match->str());
				input_cmd_id = true;
			}
			else
			{
				args.push_back(match->str());
			}

			match++;
		}
	}

	return cmd_parsed_package(ID, args);
}

Command::~Command()
{
	this->_command.clear();
}