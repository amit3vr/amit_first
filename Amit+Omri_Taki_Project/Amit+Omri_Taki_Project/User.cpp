#include <iostream>
#include <Windows.h>
#include "TakiCore.h"
#include "Room.h"
#include "User.h"

using namespace std;
using namespace TakiConstants::UserConfig;
using namespace TakiAPI;

User::User(SOCKET socket, string username) : _socket(socket), _room(NULL)
{
	if (username.size() > MAX_UNAME_SIZE)
	{
		username.resize(MAX_UNAME_SIZE);
	}

	this->_username = username;
}

bool User::enter(Room* room)
{
	try
	{
		if (!room)
			throw(Errors::PGM_ERR_ROOM_NOT_FOUND);

		room->add(*this);
	}
	catch (cmd_id)
	{
		return false;
	}

	this->_room = room;
	return true;
}

bool User::exit()
{
	if (!this->_room)
	{
		return false;
	}

	this->_room->remove(this->_socket);
	this->_room = NULL;

	return true;
}

SOCKET User::socket() const
{
	return this->_socket;
}

string User::username() const
{
	return this->_username;
}

Room* User::room() const
{
	return this->_room;
}

bool User::is_admin() const
{
	if (this->_room)
	{
	//	return ((*this) == this->_room->admin());
		return true;
	}

	return false;
}

bool User::operator== (const User& compared)
{
	return (this->username().compare(compared.username()) == 0);
}

bool User::operator!= (const User& compared)
{
	return (!((*this) == compared));
}

User::~User()
{
	this->_username.clear();
}