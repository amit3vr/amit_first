#ifndef _ROOM
#define _ROOM

#include <iostream>
#include <Windows.h>
#include <vector>
#include <map>
#include "TakiCore.h"
#include "Deck.h"
#include "Card.h"

/* forward-decleration dependency */
#ifndef _USER
	class User;
#endif

/* extentions */
using std::vector;
using std::map;
using std::string;
using TakiAPI::Command;
using TakiConstants::RoomConfig::TurnMod;

using Hand = std::vector<Card*>;
using Player = std::pair<User*, Hand>;

class Room
{
	private:
		string				_name;
		unsigned int		_game_id;

		bool				_in_game;
		vector<Player*>		_players;
		Deck				_deck;

		int					_current;
		TurnMod				_turn_modifier;
		int					_draw_counter;

		size_t				_count_turns;

		vector<Player*>::const_iterator find(SOCKET) const;

	public:
		Room(string name);
		~Room();

		void add(User&);
		void remove(SOCKET);

		bool is_open() const;
		bool is_inside(SOCKET) const;
		bool is_current_holding(Card&) const;

		string name() const;
		User* admin() const;
		size_t num_of_players() const;
		unsigned int game_id() const;
		size_t turns_number() const;

		void set_game_id(unsigned int);
		void send_control_msg(Command&) const;

		void start_game();
		void end_game();

		void play_turn(SOCKET, vector<Card>& moves);	// when taki-open you cant put open-taki cards
		void draw_cards(SOCKET = __UNINDENTIFIED_SOCKET);
};

#endif