#ifndef _TAKI
#define _TAKI

#include <iostream>
#include <Windows.h>
#include <regex>
#include <vector>
#include <map>
#include <thread>
#include "sqlite3.h"

/* SYSTEM SYMBOLS */
#define __UNLIMITED_CONNECTIONS 0
#define __UNINDENTIFIED_SOCKET 0

/* SYSTEM SETTINGS & CONSTANTS */
namespace TakiConstants
{
	namespace RoomConfig
	{
		static const	size_t		MAX_NAME_SIZE = 20;

		static const	size_t		PLAYERS_PER_ROOM = 10;
		static const	size_t		MIN_FOR_GAME = 2;

		static const	size_t		CARDS_PER_PLAYER = 8;

		static const enum TurnMod : int
		{
			REVERSE_STOP = -2,
			REVERSE,
			NORMAL = 1,
			NORMAL_STOP,
		};
	}

	namespace UserConfig
	{
		static const	size_t		MAX_UNAME_SIZE = 10;
		static const	size_t		MAX_PASSW_SIZE = 20;
	}

	namespace CardConfig
	{
		static const enum TYPE : char
		{
			PLUS = '+',
			STOP = '!',
			CHNG_CLR = '%',
			CHNG_DIR = '<',
			DOUBLE = '$',
			TAKI = '^',
			SUPER_TAKI = '*'
		};

		static const enum COLOR : char
		{
			UNDEF = 0,
			RED = 'r',
			GREEN = 'g',
			BLUE = 'b',
			YELLOW = 'y'
		};
	}
}

namespace TakiDB
{
	namespace
	{
		using std::string;
		using std::map;

		using query_parsed_record = map<string, string>;
		using query_parsed_package = map<int, query_parsed_record>;
	}

	namespace TakiConfig
	{
		static const	string		DB_FILE_NAME = "Taki.db";

		static const	bool		RESTART_DB = true;
		static const	bool		DEBUG_MODE = true;

		static const	string		HOST_IP = "192.168.56.1";
		static const	int			PORT = 27015;

		static const	int			MAX_CONNECTIONS = __UNLIMITED_CONNECTIONS;
		static const	size_t		MAX_COMMAND_SIZE = 250;

		static const	int			CLIENT_TIMEOUT_SEC = 3;	// NOT WORKING
	}

	class Database
	{
		private:
			sqlite3*	_db;

			bool		_debug;
			bool		_restart;
			bool		_status;

			static int parse(void*, int, char**, char**);

		public:
			Database(	string db_file_name,
						bool restart = TakiConfig::RESTART_DB,
						bool debug = TakiConfig::DEBUG_MODE
					);

			~Database();

			bool exec(string query, query_parsed_package* = NULL);

			sqlite3_int64 last_insert_id() const;

			int errcode() const;
			string errmsg() const;
	};
}

namespace TakiAPI
{
	namespace
	{
		using std::string;
		using std::pair;
		using std::thread;
		using std::vector;
		using std::regex;

		using cmd_id = int;

		using client_proc_package = pair<SOCKET, thread*>;
		using cmd_parsed_package = pair<cmd_id, vector<string>>;
	}

	static const enum Requests : cmd_id
	{
		EN_REGISTER = 1,
		EN_LOGIN,
		EN_LOGOUT,

		RM_ROOM_LIST = 10,
		RM_CREATE_GAME,
		RM_JOIN_GAME,
		RM_START_GAME,
		RM_LEAVE_GAME,
		RM_CLOSE_GAME,

		GM_PLAY = 20,
		GM_DRAW,

		CH_SEND = 30

	};

	static const enum Responses : cmd_id
	{
		PGM_SCC = 100,
		PGM_SCC_LOGIN,
		PGM_SCC_REGISTER,
		PGM_SCC_GAME_CREATED,
		PGM_SCC_GAME_JOIN,
		PGM_SCC_GAME_CLOSE,

		PGM_CTR = 110,
		PGM_CTR_NEW_USER,
		PGM_CTR_REMOVE_USER,
		PGM_CTR_GAME_STARTED,
		PGM_CTR_ROOM_CLOSED,
		PGM_CTR_ROOM_LIST,

		GAM_SCC = 200,
		GAM_SCC_TURN,
		GAM_SCC_DRAW,

		GAM_CTR = 210,
		GAM_CTR_TURN_COMPELTE,
		GAM_CTR_DRAW_CARDS,
		GAM_CTR_GAME_ENDED,

		CHA_SCC = 300
	};

	static const enum Errors : cmd_id
	{
		PGM_ERR = 120,
		PGM_ERR_LOGIN,
		PGM_ERR_REGISTERR_INFO,
		PGM_ERR_NAME_TAKEN,
		PGM_ERR_ROOM_FULL,
		PGM_ERR_ROOM_NOT_FOUND,
		PGM_ERR_TOO_FEW_USERS,
		PGM_ERR_INFO_TOO_LONG,
		PGM_ERR_GAME_CREATED,

		PGM_MER = 130,
		PGM_MER_MESSAGE,
		PGM_MER_ACCESS,

		GAM_ERR = 220,
		GAM_ERR_ILLEGAL_CARD,
		GAM_ERR_ILLEGAL_ORDER,
		GAM_ERR_LAST_CARD,
		GAM_ERROR_WRONG_DRAW,

		CHA_ERR = 310
	};

	namespace CMDParser
	{

		static const	char*		END_CMD = NULL;
		static const	int			CMD_ID_GR = 0;

		static const	cmd_id		CMD_ERROR = -1;
		static const	cmd_id		CMD_UNDEFINED = 0;

		static const	regex		CMD_TEMPLATE("@(\\d+)\\|(?:(.+?)\\|)*\\|");
		static const	regex		CMD_ARG("@?(?:(.+?)[|])");
	}

	class Command
	{
		private:
			string	_command;

			void init(cmd_id, ...);

		public:
			Command(cmd_id, const char*, ...);
			Command(cmd_id, string, ...);
			Command(cmd_id);
			Command(string cmd = "");

			~Command();

			bool send_to(SOCKET) const;
			cmd_parsed_package parse();
	};
}

#endif