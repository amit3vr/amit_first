#include <iostream>
#include "TakiCore.h"
#include "TakiModules.h"

void main(int argc, char** argv)
{
	if (string(argv[argc-1]) == "-s")
		TakiModules::Server();
	else
		TakiModules::DemoClient();

	system("PAUSE");
}