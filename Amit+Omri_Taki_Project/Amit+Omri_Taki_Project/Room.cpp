#include <iostream>
#include <vector>
#include "Room.h"
#include "User.h"
#include "Card.h"
#include "Deck.h"
#include "TakiCore.h"

using namespace TakiConstants::RoomConfig;
using namespace TakiConstants::CardConfig;
using namespace TakiAPI;

Room::Room(string name) : _in_game(false), _game_id(0)
{
	if (name.size() > MAX_NAME_SIZE)
	{
		name.resize(MAX_NAME_SIZE);
	}

	this->_name = name;

	// Set users vector
	this->_players.resize(PLAYERS_PER_ROOM);
	this->_players.shrink_to_fit();

	this->_players.assign(PLAYERS_PER_ROOM, NULL);

	// Reset counters and modifiers
	this->_count_turns = 0;
	this->_current = 0;
	this->_draw_counter = 0;
	this->_turn_modifier = NORMAL;
}

void Room::add(User& user)
{
	for (size_t i = 0; i < this->_players.size(); i++)
	{
		if (!this->_players[i])
		{
			this->_players[i] = new Player(&user, Hand());

			this->send_control_msg(Command(Responses::PGM_CTR_NEW_USER,
									user.username()));
			return;
		}
	}
	
	throw(Errors::PGM_ERR_ROOM_FULL);
}

void Room::remove(SOCKET user_sock)
{
	// CHECK FOR EXCEPTIONS!

	if (this->admin()->socket() == user_sock)
	{
		this->end_game();
		return;
	}

	for (size_t i = 0; i < this->_players.size(); i++)
	{
		if (this->_players[i] &&
			this->_players[i]->first->socket() == user_sock)
		{
			this->send_control_msg(Command(Responses::PGM_CTR_REMOVE_USER,
											this->_players[i]->first->username()));

			delete this->_players.at(i);
			this->_players[i] = NULL;
		}
	}
}

void Room::play_turn(SOCKET s, vector<Card>& moves)
{
	User& current_player = *(this->_players[_current]->first);
	bool is_open_taki = false, put_another = true;

	// Reset 'STOP' flag effects.
	if (this->_turn_modifier == NORMAL_STOP)
		this->_turn_modifier = NORMAL;
	else if (this->_turn_modifier == REVERSE_STOP)
		this->_turn_modifier = REVERSE;

	if (!this->_in_game)
	{
		throw(Errors::PGM_MER_ACCESS);
		return;
	}

	if (current_player.socket() != s)
	{
		throw(Errors::PGM_ERR_LOGIN);
		return;
	}

	if (moves.empty())
	{
		throw(Errors::GAM_ERR_ILLEGAL_CARD);
		return;
	}

	for each(Card turn in moves)
	{
		if (!this->is_current_holding(turn))
		{
			continue;
		}

		if (put_another)
			put_another = is_open_taki;
		else
			break;

		if (this->_deck.current() != turn)
		{
			this->draw_cards();

			throw(Errors::GAM_ERR_ILLEGAL_CARD);
			return;
		}

		switch (turn.get_type())
		{
			case TYPE::DOUBLE:
				this->_draw_counter += 2;
			break;

			case TYPE::CHNG_DIR:
				this->_turn_modifier = this->_turn_modifier > 0 ? REVERSE : NORMAL;
			break;

			case TYPE::PLUS:
				put_another = true;
			break;

			case TYPE::STOP:
				this->_turn_modifier = this->_turn_modifier > 0 ? NORMAL_STOP : REVERSE_STOP;
			break;

			case TYPE::TAKI:
			case TYPE::SUPER_TAKI:
				is_open_taki = true;
			break;
		}

		this->_deck.place(turn);
	}

	if (this->_players[_current]->second.empty())
	{
		// WINNER

	}
	
	this->send_control_msg(Command(GAM_CTR_TURN_COMPELTE));

	this->_current = (this->_current + this->_turn_modifier) % PLAYERS_PER_ROOM;
	this->_count_turns++;

	moves.clear();

	Command(Responses::GAM_SCC_TURN).send_to(s);
}

size_t Room::turns_number() const
{
	return this->_count_turns;
}

vector<Player*>::const_iterator Room::find(SOCKET s) const
{
	for (auto i = this->_players.begin(); i < this->_players.end(); i++)
	{
		if ((*i)->first->socket() == s)
		{
			return i;
		}
	}
	
	return this->_players.end();
}

string Room::name() const
{
	return this->_name;
}

size_t Room::num_of_players() const
{
	size_t i;

	for (i = 0; i < this->_players.size(); i++)
	{
		if (this->_players.at(i))
			i++;
		else
			break;
	}

	return i;
}

void Room::send_control_msg(Command& cmd) const
{
	for each (Player* player in this->_players)
	{
		if (player)
		{
			cmd.send_to(player->first->socket());
		}
	}
}

void Room::draw_cards(SOCKET s)
{
	Hand* player_hand = NULL;

	if (s == __UNINDENTIFIED_SOCKET)
	{
		player_hand = &(this->_players[this->_current]->second);
	}
	else if (this->is_inside(s))
	{
		player_hand = &((*this->find(s))->second);
	}
	else
	{
		throw(Errors::GAM_ERROR_WRONG_DRAW);
		return;
	}

	for (int i = 0; i < this->_draw_counter; i++)
	{
		player_hand->push_back(&this->_deck.draw());
	}

	this->_draw_counter = 0;
	throw(Responses::GAM_SCC_DRAW);
}

bool Room::is_inside(SOCKET s) const
{
	return (this->find(s) != this->_players.end());
}

bool Room::is_current_holding(Card& c) const
{
	for each (Card* card in this->_players[_current]->second)
	{
		if (c.get_color() == card->get_color() &&
			c.get_type() == card->get_type())
		{
			return true;
		}
	}

	return false;
}

void Room::start_game()
{
	if (!this->_in_game && 
		this->num_of_players() >= MIN_FOR_GAME)
	{
		this->_in_game = true;

		this->send_control_msg(Command(Responses::PGM_CTR_GAME_STARTED,
								std::to_string(this->num_of_players()))
						);
	}
	else
	{
		throw(Errors::PGM_ERR_TOO_FEW_USERS);
	}
}

void Room::end_game()
{
	if (!this->_in_game)
	{
		throw(TakiAPI::CMDParser::CMD_ERROR);
	}
	else
	{
		this->_in_game = false;
		this->send_control_msg(Command(Responses::PGM_CTR_ROOM_CLOSED));

		for each (Player* player in this->_players)
		{
			if (player)
			{
				player->first /* pointer to the player's User instance */
					->exit();
			}
		}

		throw(Responses::GAM_CTR_GAME_ENDED);
	}
}

void Room::set_game_id(unsigned int game_id)
{
	this->_game_id = game_id;
}

unsigned int Room::game_id() const
{
	return this->_game_id;
}

User* Room::admin() const
{
	for each(Player* player in this->_players)
	{
		if (player)
		{
			return player->first;
		}
	}

	return NULL;
}

Room::~Room()
{
	this->_name.clear();
	this->_players.clear();

	this->_deck.~Deck();
}